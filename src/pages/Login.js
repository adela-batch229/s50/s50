import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Login() {

	// allow us to consume the UserContext object and its properties
	const {user, setUser} = useContext(UserContext);
	
	// State hooks -> store values of the input fields
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password)

	useEffect(() => {
		// Validation
		if((email !== '' && password !== '')){
			setIsActive(true)
		} else{
			setIsActive(false)
		}
	}, [email, password]);

	// Function to simulate user registration
	function authenticate(e){
		// prevents page redirection via form submission
		e.preventDefault();


		//Process a fetch request to the corresponding backend API 
		/*
			Syntax:
				fetch('url', {options})
				.then(res => res.json())
				.then(data => {})
		*/
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data)

			// If no user information is found, the "access" property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					icon:"success",
					title: "Login Successful!",
					text: "Welcome to Zuitt."
				})
			} else {
				Swal.fire({
					icon:"error",
					title: "Authentication Failed!",
					text:" Check credentials and try again."
				})
			}

		})

		//allows us to set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem('propertyName', value)

		*/
		/*localStorage.setItem('email', email);

		setUser({
			email: localStorage.getItem('email')
		})*/

		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) => {
		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow implementation standards for JWTs


		fetch('http://localhost:4000/users/details', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}



	return(

	(user.id !== null)?

		<Navigate to="/courses"/>
		:
		<>
			<h1>Login</h1>
			<Form onSubmit = {(e) =>authenticate(e)}>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="name@mail.com" 
		        	value={email} 
		        	onChange = {e => setEmail(e.target.value)} 
		        	required
		        />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password" 
		        	value={password} 
		        	onChange = {e => setPassword(e.target.value)} 
		        	required
		        />
		      </Form.Group>


		      {/*Conditional rendering -> if active, button is clickable, if inactive, button is not clickable*/}
		      {
		      	(isActive) ? 
		      	<Button variant="success" type="submit" controlId="submitBtn">
		        Login
		      	</Button>
		      	:
		      	<Button variant="danger" type="submit" controlId="submitBtn" disabled>
		        Login
		      	</Button>
		      }	      
		    </Form>
	    </>
		)
}