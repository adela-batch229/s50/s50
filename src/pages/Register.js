import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react'
import {Navigate, Link, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'

export default function Register() {
	
	// State hooks -> store values of the input fields

	const navigate = useNavigate()

	const {user, setUser} = useContext(UserContext);


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	console.log(email);
	console.log(password1)
	console.log(password2)

	useEffect(() => {
		// Validation
		if((firstName !== '' && lastName !== '' && mobileNo !== '' &&email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true)
		} else{
			setIsActive(false)
		}
	})

	// Function to simulate user registration
	const registerUser = (e) => {
		e.preventDefault();
		// prevents page redirection via form submission
		fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail`,{
			method:"POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === false){
				fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data=> {
					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Welcome!",
							icon: "success",
							text: "You have Successfully Registered!"
						})
						navigate("/login")
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})

			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Email is already used!"
				})				
			}
		})
		
	}
		/*setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');*/


	return(
		(user.id !== null)?

			<Navigate to = "/courses"/>
			:
			<Form onSubmit = {(e) =>registerUser(e)}>
			  <h1>Register</h1>
			  <Form.Group className="mb-3" controlId="userFName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control type="text" placeholder="First name" value={firstName} onChange = {e => setFirstName(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userLName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control type="text" placeholder="Last name" value={lastName} onChange = {e => setLastName(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userMobile">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control type="number" placeholder="09xxxxxxxxx" value={mobileNo} onChange = {e => setMobileNo(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="name@mail.com" value={email} onChange = {e => setEmail(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password1} onChange = {e => setPassword1(e.target.value)} required/>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control type="password" placeholder="Re-enter Password" value={password2} onChange = {e => setPassword2(e.target.value)} />
		      </Form.Group>


		      <Form.Group className="mb-3" controlId="termsCheckBox">
		        <Form.Check type="checkbox" label="I agree to terms and condition" required/>
		      </Form.Group>

	      {/*Conditional rendering -> if active, button is clickable, if inactive, button is not clickable*/}
	      {
	      	(isActive) ? 
	      	<Button variant="primary" type="submit" controlId="submitBtn">
	        Register
	      	</Button>
	      	:
	      	<Button variant="primary" type="submit" controlId="submitBtn" disabled>
	        Register
	      	</Button>
	      }
	      
	    </Form>
		)
}