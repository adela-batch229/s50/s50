// Destructuring components/modules for cleaner codebase
import {Button, Row, Col} from 'react-bootstrap'

export default function Banner(){
	return(
		<Row>
			<Col className = "text-center p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere!</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		)
}