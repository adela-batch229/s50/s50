import {Card, Button} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProps}){

	// check to see if the data was successfully passed
	// console.log(courseProps)
	// console.log(typeof courseProps);

	//destructuring the data to avoid dot notation
	const {name, description, price, _id} = courseProps;

	//3 hooks in react
	//1. useState
	//3. useEffect
	//4. useContext

	// Use the useState hook for the component to be able to store state
	// States are used to keep track of information related to individual components
	// Syntax -> const [getter, setter] = useState(initialGetterValue);

	const [count,setCount] = useState(0);
	const [seat, setSeat] = useState(30)

	/*function enroll(){
		if(seat === 0){
			alert("No more Seats")
		}else {
			setCount(count + 1);
			console.log("Enrollees " + count)
			setSeat(seat - 1);
		}
	}*/
	
	return(
	    <Card className = 'courseCard mx-4 my-3 p-3'>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>Enrollees: {count}</Card.Text>
	            <Card.Text>Slots Available: {seat}</Card.Text>
	            <Link className="btn btn-primary" to ={`/courseView/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>

		)
}